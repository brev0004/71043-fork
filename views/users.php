<?php
/**
 * Users.php - renders, well, it should render a list of users
 *
 * @author Bugslayer
 *
 */

// Check if the request is done by an authorized user. If not, show 401.php and exit
if (!isAuthenticated()) {
	include '401.php';
	exit();
}
?>
<h1>Gebruikers</h1>

<?php
echo "<table style='border: solid 1px black;'>";
echo "<tr><th>Gebruikersnaam</th><th>Wachtwoord</th><th>Voornaam tussenvoegsel en achternaam </th><th><th><th>Emailadres<th>Telefoonnummer<th>Straat<th>Nummer<th>Postcode<th>Plaats</th></th></th></th></th></th></th></th>


</tr>";

class TableRows extends RecursiveIteratorIterator {
	function __construct($it) {
		parent::__construct($it, self::LEAVES_ONLY);
	}

	function current() {
		return "<td style='width:150px;border:1px solid black;'>" . parent::current() . "</td>";
	}

	function beginChildren() {
		echo "<tr>";
	}

	function endChildren() {
		echo "</tr>" . "\n";
	}

}

$servername = "localhost";
$username = "mbvolley";
$password = "mbvolley";
$dbname = "project";

try {
	$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
	$conn -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$stmt = $conn -> prepare("SELECT * FROM user Order by name_last ASC ");
	$stmt -> execute();

	// set the resulting array to associative
	$result = $stmt -> setFetchMode(PDO::FETCH_ASSOC);
	foreach (new TableRows(new RecursiveArrayIterator($stmt->fetchAll())) as $k => $v) {
		echo $v;
	}
} catch(PDOException $e) {
	echo "Error: " . $e -> getMessage();
}
$conn = null;
echo "</table>";
?>