<?php
/**
 * Menu.php - renders the menu dynamically.
 * 
 * @author Bugslayer
 * 
 */
// Include required external scripts
require_once dirname ( __FILE__ ) . '/db.php';

global $page; // Gets the global variable which is set in index.php

/**
 * Returns an array with relevant info of the articles in the database
 */
function listArticleMetaInfo() {
	global $mysqli;
	$sql = "SELECT ID, Name FROM ARTICLE;";
	$result = $mysqli->query ( $sql );
	$names = array ();
	while ( $row = $result->fetch_assoc () ) {
		$data = array ();
		$data ['ID'] = $row ['ID'];
		$data ['Name'] = $row ['Name'];
		$names [] = $data;
	}
	return $names;
}

?>
<ul class="navbar-top">
	<li><a href="index.php">Home</a></li>
	<!-- het menu met de lijst artikelen -->
	<li><a href="#">Informatie</a>
		<ul>
        	<?php foreach ( listArticleMetaInfo () as $articleMetaInfo ) {
					echo '<li><a href="?action=show&page=article&id=' . $articleMetaInfo ['ID'] . '">' . $articleMetaInfo ['Name'] . '</a></li>';
				  }
			?>
        </ul>
    </li>
    <?php if (isAuthenticated()) { // This menu is only visible when the user is logged in. ?>
		<li><a href="#">Beheer</a>
			<ul>
				<li><a href="?action=show&page=article&mode=new">Nieuw artikel</a></li>
		            <?php
						// Menu items voor het bewerken van de content. Is wel afhankelijk van of de pagina een artikel laat zien
						if ($page == 'article' && isset ( $_GET ['id'] )) {
							// Pagina laat een bepaald artikel zien. Dus edit en remove zijn links
							echo '<li><a href="?action=show&page=article&mode=edit&id=' . $_GET ['id'] . '">Bewerk artikel</a></li>';
							echo '<li><a href="?action=show&page=article&mode=delete_confirm&id=' . $_GET ['id'] . '">Artikel verwijderen</a></li>';
						} else {
							// Geen @@project artikel: links disablen
							echo '<li><a class="disabled" href="#">Bewerk artikel</a></li>';
							echo '<li><a class="disabled" href="#">Artikel verwijderen</a></li>';
						}
						?>
		        </ul>
		 </li>
    <?php } //end the if statement ?>
    <!-- Menu items for the other pages -->
	<li><a href="?action=show&page=contact">Contact</a></li>
	<li><a href="?action=show&page=Adresgegevens">Adresgegevens</a></li>
	<li><a href="?action=show&page=register">Registreren</a></li>
	<li><a href="?action=show&page=search">Zoek</a></li>
	<li><a href="?action=show&page=order">Bestellen</a></li>
    <?php if (isAuthenticated()) {  // This menu is only visible when the user is logged in. ?>
	    <li><a href="?action=show&page=users">Gebruikersbeheer</a></li>
    <?php } //end the if statement ?>
    
</ul>