<?php
/**
 * Index.php - main page of the application. All requests should land here, except for ajax calls
 * 
 * @author Bugslayer
 * 
 */
 
// This global variable represents the database connection
$mysqli = null;

// Mysql connection settings
$url      = 'localhost';
$userid   = 'mbvolley'  ;
$password = 'mbvolley'  ;
$database = 'project'  ;

// Instantiate the database object.
$mysqli = new mysqli ( $url, $userid, $password, $database );
if ($mysqli->connect_errno) {
	die ( "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error );
}

/**
 * Returns the global database connection
 */
function getMysqli() {
	global $mysqli;
	return $mysqli;
}

?>