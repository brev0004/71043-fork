<?php
/**
 * Order.php - Controller for the order page. Saves orders to the database
 * 
 * @author Bugslayer
 * 
 */
// Include required external scripts
require_once dirname ( __FILE__ ) . '/../components/db.php';
include_once dirname ( __FILE__ ) . '/../components/datatools.php';
include_once dirname ( __FILE__ ) . '/../components/formvalidationtools.php';

global $action; // set in index.php

// Determine and process the action.
switch ($action) {
	case "save" :
		save_order ();
		header ( "Location: index.php" );
		break;
	default :
		die ( "Illegal action" );
}

function save_order() {
	// Step 1: check is all required fields are sent
	if ( ! isset ( $_POST ['slevels'] ) || ! isset ( $_POST ['wigbekken'] ) || ! isset ( $_POST ['zwenkmoeren'] )) {
		printErrorAndDie ( 'Het lijkt er op dat het formulier dat u gebruikt niet klopt.' );
	}
	
	// step 2: safely transfer the post-data to local variables
	$slevels = strip_tags ( $_POST ['Customerid'] );
	$slevels = strip_tags ( $_POST ['slevels'] );
	$wigbekken = strip_tags ( $_POST ['wigbekken'] );
	$zwenkmoeren = strip_tags ( $_POST ['zwenkmoeren'] );
	
	// Step 3: input validation
	$error_message = "";
	$error_message .= validateNumber ( $slevels, 'Het aantal slevels is niet ingevuld.' );
	if ($slevels<0)
		$error_message .= "Het aantal slevels is negatief.";
	$error_message .= validateNumber ( $wigbekken, 'Het aantal wigbekken is niet ingevuld.' );
	if ($wigbekken<0)
		$error_message .= "Het aantal wigbekken is negatief.";
	$error_message .= validateNumber ( $zwenkmoeren, 'Het aantal zwenkmoeren is niet ingevuld.' );
	if ($zwenkmoeren<0)
		$error_message .= "Het aantal zwenkmoeren is negatief.";
	// Validation has errors when the length of error_message > 0
	if (strlen ( $error_message ) > 0) {
		printErrorAndDie ( $error_message );
	}
	
	// Step 4: get the userid of the authenticated user. He is the one placing the order, so he should pay
	$customerID = null;
	if (isset($_SESSION['Username']))
		$customerID = $_SESSION['Username'];
	$date = date_create()->format('Y-m-d H:i:s');
	
	// Step 5: call addOrder to add the order to the database
	addOrder ( $customerID, $date, $slevels, $wigbekken, $zwenkmoeren );
}

/**
 * Adds an order to the database.
 * 
 * @param unknown $customerID
 * @param unknown $date
 * @param unknown $slevels
 * @param unknown $wigbekken
 * @param unknown $zwenkmoeren
 */
function addOrder( $customerID, $date, $slevels, $wigbekken, $zwenkmoeren ) {
	global $mysqli;
	$sql = "INSERT INTO `ORDER` (`CustomerID`, `Date`, `Slevels`, `Wigbekken`, `Zwenkmoeren`) "
			."VALUES ('$customerID', '$date', $slevels, $wigbekken, $zwenkmoeren);";
	echo $sql;
	if (! $mysqli->query ( $sql )) {
		die ( "Errormessage: ". $mysqli->error );
	}
	
}
